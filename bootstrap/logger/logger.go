package logger

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"nodemessage.com/x-notify/bootstrap/util"
	"os"
	"path"
	"path/filepath"
	"time"
)

var Instinct = newLogger()

type logFormatter struct {
	PreText string
}

func (m *logFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	timestamp := entry.Time.Format("2006-01-02 15:04:05.000")
	var msg string
	if entry.HasCaller() {
		fName := filepath.Base(entry.Caller.File)
		msg = fmt.Sprintf("%s [%s] [%-7s] [%s:%d %s]: %s\n",
			m.PreText, timestamp, entry.Level.String(), fName, entry.Caller.Line, entry.Caller.Function, entry.Message)
	} else {
		msg = fmt.Sprintf("%s [%s] [%s] %s\n", m.PreText, timestamp, entry.Level, entry.Message)
	}

	b.WriteString(msg)
	return b.Bytes(), nil
}

func newLogger() *logrus.Logger {
	now := time.Now()
	logFilePath := filepath.Join(util.BinAbsPath(), "logs")
	if err := os.MkdirAll(logFilePath, 0777); err != nil {
		fmt.Println(err.Error())
	}
	logFileName := now.Format("2006-01-02") + ".log"
	//日志文件
	fileName := path.Join(logFilePath, logFileName)
	if _, err := os.Stat(fileName); err != nil {
		if _, err := os.Create(fileName); err != nil {
			fmt.Println(err.Error())
		}
	}
	//写入文件
	src, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Println("err", err)
	}

	writers := io.MultiWriter(src, os.Stdout)
	//实例化
	logger := logrus.New()

	//设置输出
	logger.Out = writers

	//设置日志级别
	logger.SetLevel(logrus.DebugLevel)

	//设置日志格式
	logger.SetFormatter(&logFormatter{PreText: "[ X-notify ]"})
	return logger
}

func Info(format string, args ...interface{}) {
	if len(args) > 0 {
		Instinct.Infof(format, args)
	} else {
		Instinct.Info(format)
	}
}

func Error(format string, args ...interface{}) {
	if len(args) > 0 {
		Instinct.Errorf(format, args)
	} else {
		Instinct.Error(format)
	}
}

func Debug(format string, args ...interface{}) {
	if len(args) > 0 {
		Instinct.Debugf(format, args)
	} else {
		Instinct.Debug(format)
	}
}
