package bootstrap

import (
	"github.com/urfave/cli"
	"net/http"
	"nodemessage.com/x-notify/asset"
	"nodemessage.com/x-notify/bootstrap/build"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/conf"
	"nodemessage.com/x-notify/templates"
)

func command() *cli.App {
	app := cli.NewApp()
	app.Name = "x-notify"
	app.Usage = "实现消息通知业务"
	app.Version = build.Version()
	app.Copyright = "Copyright (c) 2024, 2024, x-notify. All rights reserved."
	app.Authors = []cli.Author{
		{
			Name:  "wjsmc",
			Email: "wjsmcemail@163.com",
		},
	}
	app.Before = func(ctx *cli.Context) (err error) {
		return nil
	}
	app.Commands = commands
	return app
}

var bootstrap *Bootstrap

// run 启动web
func run(ctx *cli.Context) (err error) {
	bootstrap = Application()
	if bootstrap == nil {
		logger.Error("项目启动失败,请查看详细日志")
		return nil
	}
	bootstrap.TemplatesFs(templates.Templates, "*")
	bootstrap.StaticFs("/asset", http.FS(asset.FileServer))
	bootstrap.Start()
	return nil
}

// initPro  初始化web
func initPro(ctx *cli.Context) {
	if err := conf.InitConkCli(); err != nil {
		logger.Error("初始化出错:%v", err)
	}
}
