package config

var (
	config Config
)

type Model string

var (
	MODEL_DEV     = "debug"
	MODEL_TEST    = "test"
	MODEL_RELEASE = "release"
)

type Config struct {
	Model        Model    `mapstructure:"model"`
	TokenKey     string   `mapstructure:"token_key"`
	PassAuthRegx []string `mapstructure:"pass_auth_regx"`
	Addr         string   `mapstructure:"addr"`
	DbPath       string   `mapstructure:"db_path"`
}

func Properties() Config {
	return config
}

func Initialize(conf Config) {
	config = conf
}
