package bootstrap

import "github.com/urfave/cli"

var (
	commands = []cli.Command{
		{
			Name:   "run",
			Usage:  "运行web项目",
			Action: run,
		},
		{
			Name:   "init",
			Usage:  "初始化项目",
			Action: initPro,
		},
	}
)
