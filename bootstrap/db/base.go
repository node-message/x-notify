package db

import (
	"reflect"
)

type Where struct {
	TableName string
	Sql       string
	Arg       []any
}

func SelectOne(where any) (data any) {
	if w, ok := where.(Where); ok {
		data = reflect.New(reflect.TypeOf(where)).Interface()
		Instinct().Table(w.TableName).Where(w.Sql, w.Arg).First(&data)
	} else {
		data = reflect.New(reflect.TypeOf(where)).Interface()
		Instinct().Where(where).First(&data)
	}
	return
}

func Select(where any, data any) (rows int64) {
	if w, ok := where.(Where); ok {
		tx := Instinct().Table(w.TableName).Where(w.Sql, w.Arg).Find(data)
		rows = tx.RowsAffected
	} else {
		tx := Instinct().Debug().Where(where).Find(data)
		rows = tx.RowsAffected
	}
	return
}

func Insert(data any) (int64, error) {
	tx := Instinct().Create(data)
	return tx.RowsAffected, tx.Error
}

func UpdateWhere(where any, data any) (int64, error) {
	if w, ok := where.(Where); ok {
		tx := Instinct().Table(w.TableName).Where(w.Sql, w.Arg).Updates(data)
		return tx.RowsAffected, tx.Error
	} else {
		tx := Instinct().Where(where).Updates(data)
		return tx.RowsAffected, tx.Error
	}
}

func DeleteWhere(where Where) (int64, error) {
	tx := Instinct().Table(where.TableName).Delete(where.Sql, where.Arg)
	return tx.RowsAffected, tx.Error
}

func DeleteWhereModel(where Where, mode any) (int64, error) {
	tx := Instinct().Delete(mode, where.Sql, where.Arg)
	return tx.RowsAffected, tx.Error
}
