package db

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"nodemessage.com/x-notify/bootstrap/config"
	"path/filepath"
)

var db *gorm.DB

func Instinct() *gorm.DB {
	return db
}

func Initialize() {
	orm, err := gorm.Open(sqlite.Open(filepath.Join(config.Properties().DbPath, "data.db")), &gorm.Config{})
	if err != nil {
		panic(fmt.Sprintf("数据库连接异常,%v", err))
	}
	db = orm
}
