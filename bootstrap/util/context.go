package util

import (
	"github.com/gin-gonic/gin"
	"nodemessage.com/x-notify/bootstrap/model"
)

func ContextGet(ctx *gin.Context) *model.Context {
	value, exists := ctx.Get(model.AUTH_CONTEXT)
	if !exists {
		return nil
	}
	context, ok := value.(*model.Context)
	if ok {
		return context
	} else {
		return nil
	}
}
