package util

import (
	"encoding/json"
	"reflect"
)

func JsonFormat(data any) (string, error) {
	marshal, err := json.Marshal(&data)
	if err != nil {
		return "", err
	}
	return string(marshal), nil
}

func JsonParse(jsonStr string, conf any) error {
	kind := reflect.ValueOf(conf).Kind()
	var err error
	if kind == reflect.Ptr {
		err = json.Unmarshal([]byte(jsonStr), conf)
	} else {
		err = json.Unmarshal([]byte(jsonStr), &conf)
	}
	if err != nil {
		return err
	}
	return nil
}
