package util

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

func TokenCreate(key []byte, payload map[string]any) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["iss"] = "bootstrap"
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Minute * 10).Unix()
	claims["payload"] = payload
	return token.SignedString(key)
}

func TokenCreateJson(key []byte, data any) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["iss"] = "bootstrap"
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Minute * 10).Unix()
	parse, err := JsonFormat(data)
	if err != nil {
		return "", err
	}
	claims["payload"] = parse
	return token.SignedString(key)
}

func TokenParse(key []byte, token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
}

func TokenPayload(claims jwt.MapClaims) map[string]any {
	v := claims["payload"]
	if v == nil {
		return map[string]any{}
	}
	return claims["payload"].(map[string]any)
}

func TokenPayloadJson(claims jwt.MapClaims, payload any) error {
	v := claims["payload"]
	if json, ok := v.(string); ok {
		return JsonParse(json, payload)
	}
	return nil
}
