package filter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/bootstrap/router"
)

func exceptionFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				logger.Error("捕获异常: %v\n", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, router.Result{
					Code: router.Error,
					Msg:  "系统异常",
				})
			}
		}()
		c.Next()
	}
}
