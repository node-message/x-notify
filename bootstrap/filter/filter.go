package filter

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

var filterInner map[int]gin.HandlerFunc

func Initialize(engine *gin.Engine) {
	engine.Use(exceptionFilter())
	engine.Use(loggerFilter())
	engine.Use(authFilter())
	for _, f := range filterInner {
		engine.Use(f)
	}
}

func Append(order int, filter gin.HandlerFunc) {
	handlerFunc := filterInner[order]
	if handlerFunc != nil {
		panic(fmt.Sprintf("过滤器已存在 orer:%d", order))
	} else {
		filterInner[order] = filter
	}
}
