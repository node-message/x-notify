package filter

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"nodemessage.com/x-notify/bootstrap/config"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/bootstrap/model"
	"nodemessage.com/x-notify/bootstrap/router"
	"nodemessage.com/x-notify/bootstrap/util"
	"regexp"
)

func authFilter() gin.HandlerFunc {
	return func(context *gin.Context) {
		for _, u := range config.Properties().PassAuthRegx {
			if ok, _ := regexp.MatchString(u, context.Request.RequestURI); ok {
				context.Next()
				return
			}
		}
		token := context.GetHeader(model.TOKEN_NAME)
		if token == "" {
			router.Redirect(context, model.HTML_LOGIN)
			return
		}
		userPayload, err := util.TokenParse([]byte(config.Properties().TokenKey), token)
		if err != nil {
			logger.Error("登录失败,Token:{},%v", token, err)
			router.Redirect(context, model.HTML_LOGIN)
			return
		}
		if !userPayload.Valid {
			logger.Error("Token失效:{}", token)
			router.Redirect(context, model.HTML_LOGIN)
			return
		}

		authContext := model.NewContext()
		err = util.TokenPayloadJson(userPayload.Claims.(jwt.MapClaims), authContext)
		if err != nil {
			logger.Error("用户数据解析失败,Token:{},%v", token, err)
			router.Redirect(context, model.HTML_LOGIN)
			return
		}
		context.Set(model.AUTH_CONTEXT, authContext)
		context.Next()
	}
}
