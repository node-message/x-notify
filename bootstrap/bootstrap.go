package bootstrap

import (
	"embed"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/urfave/cli"
	"html/template"
	"net/http"
	"nodemessage.com/x-notify/bootstrap/build"
	"nodemessage.com/x-notify/bootstrap/config"
	"nodemessage.com/x-notify/bootstrap/db"
	"nodemessage.com/x-notify/bootstrap/filter"
	"nodemessage.com/x-notify/bootstrap/logger"
	_ "nodemessage.com/x-notify/bootstrap/logic"
	"nodemessage.com/x-notify/bootstrap/model"
	"nodemessage.com/x-notify/bootstrap/router"
	"os"
	"path/filepath"
	"strings"
)

var (
	runBeforeFilter []func() bool
	runConfig       func() config.Config
)

type Bootstrap struct {
	r *gin.Engine
}

func Application() *Bootstrap {
	for _, f := range runBeforeFilter {
		if !f() {
			return nil
		}
	}
	config.Initialize(runConfig())
	db.Initialize()
	b := new(Bootstrap)
	b.r = gin.Default()
	gin.SetMode(string(config.Properties().Model))
	filter.Initialize(b.r)
	router.Initialize(b.r)
	b.r.GET("/", func(context *gin.Context) {
		context.Redirect(http.StatusFound, model.HTML_INDEX)
	})
	return b
}

func (b *Bootstrap) Start() {
	if err := b.r.Run(config.Properties().Addr); err != nil {
		logger.Error("启动错误 error {}", err)
	}
}

func (b *Bootstrap) TemplatesFs(fs embed.FS, pathPattern string) {
	tmpl := template.Must(template.ParseFS(fs, pathPattern))
	b.r.SetHTMLTemplate(tmpl)
}

func (b *Bootstrap) StaticFs(path string, fs http.FileSystem) {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	b.r.StaticFS(path, fs)
}

func (b *Bootstrap) StaticFiles(path string, files ...string) {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	if !strings.HasSuffix(path, "/") {
		path = path + "/"
	}
	for _, file := range files {
		b.r.StaticFile(path+filepath.Base(file), file)
	}
}

func (b *Bootstrap) StaticFileRoot(path string, root string) {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	b.r.Static(path, root)
}

func Run(initConfig func() config.Config, runBefore ...func() bool) {
	runBeforeFilter = runBefore
	runConfig = initConfig
	app := command()
	if err := app.Run(os.Args); err != nil {
		fmt.Fprintf(os.Stderr, "[g] %s\n", err.Error())
		os.Exit(1)
	}
}

func init() {
	cli.AppHelpTemplate = fmt.Sprintf(`NAME:
	{{.Name}}{{if .Usage}} - {{.Usage}}{{end}}
 
 USAGE:
	{{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}} {{if .Commands}} command{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}{{end}}{{if .Version}}{{if not .HideVersion}}
 
 VERSION:
	%s{{end}}{{end}}{{if .Description}}
 
 DESCRIPTION:
	{{.Description}}{{end}}{{if len .Authors}}
 
 AUTHOR{{with $length := len .Authors}}{{if ne 1 $length}}S{{end}}{{end}}:
	{{range $index, $author := .Authors}}{{if $index}}
	{{end}}{{$author}}{{end}}{{end}}{{if .VisibleCommands}}
 
 COMMANDS:{{range .VisibleCategories}}{{if .Name}}
 
	{{.Name}}:{{end}}{{range .VisibleCommands}}
	  {{join .Names ", "}}{{"\t"}}{{.Usage}}{{end}}{{end}}{{end}}{{if .VisibleFlags}}
 
 GLOBAL OPTIONS:
	{{range $index, $option := .VisibleFlags}}{{if $index}}
	{{end}}{{$option}}{{end}}{{end}}{{if .Copyright}}
 
 COPYRIGHT:
	{{.Copyright}}{{end}}
`, build.Build)
}
