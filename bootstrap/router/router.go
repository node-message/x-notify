package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type Method string

const (
	M_ANY    Method = "ANY"
	M_GET    Method = http.MethodGet
	M_POST   Method = http.MethodPost
	M_PUT    Method = http.MethodPut
	M_DELETE Method = http.MethodDelete
)

type Router struct {
	modelPath string
	root      *gin.RouterGroup
}

func NewSubRouter(modelPath string, root *gin.RouterGroup) *Router {
	if !strings.HasPrefix(modelPath, "/") {
		modelPath = "/" + modelPath
	}
	if !strings.HasSuffix(modelPath, "/") {
		modelPath = modelPath + "/"
	}
	return &Router{modelPath: modelPath, root: root}
}

func NewRouter(modelPath string, root *gin.Engine) *Router {
	if !strings.HasPrefix(modelPath, "/") {
		modelPath = "/" + modelPath
	}
	if !strings.HasSuffix(modelPath, "/") {
		modelPath = modelPath + "/"
	}
	return &Router{modelPath: modelPath, root: root.Group(modelPath)}
}

func (r *Router) SubRouter(modelPath string) *Router {
	return NewSubRouter(modelPath, r.root)
}

func (r *Router) Filter(filter gin.HandlerFunc) *Router {
	return r.Filters(filter)
}
func (r *Router) Filters(filter ...gin.HandlerFunc) *Router {
	r.root.Use(filter...)
	return r
}
func (r *Router) Api(method Method, path string, h gin.HandlerFunc) *Router {
	return r.Apis(method, path, h)
}
func (r *Router) Apis(method Method, path string, h ...gin.HandlerFunc) *Router {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	if method == M_ANY {
		r.root.Any(path, h...)
	} else {
		r.root.Match([]string{string(method)}, path, h...)
	}
	return r
}

type Api func(g *gin.Engine)

var routers []Api

func Register(api Api) {
	routers = append(routers, api)
}

func Initialize(g *gin.Engine) {
	for _, api := range routers {
		api(g)
	}
}
