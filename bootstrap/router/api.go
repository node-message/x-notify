package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Status int

const (
	Success Status = 1
	Error   Status = -1
)

type Result struct {
	Code Status `json:"code"`
	Msg  string `json:"msg"`
	Data any    `json:"data"`
}

func Response(result Result, c *gin.Context) {
	if result.Code == 0 {
		c.AbortWithStatusJSON(http.StatusOK, Result{
			Code: Error,
			Msg:  "程序内部错误的返回",
		})
		return
	}
	c.JSON(http.StatusOK, result)
	c.Abort()
}

func Ok(c *gin.Context) {
	c.JSON(http.StatusOK, Result{
		Code: Success,
		Msg:  "成功",
	})
	c.Abort()
}

func OkPayload(c *gin.Context, payload any) {
	c.JSON(http.StatusOK, Result{
		Code: Success,
		Msg:  "成功",
		Data: payload,
	})
	c.Abort()
}

func Fail(c *gin.Context) {
	c.JSON(http.StatusOK, Result{
		Code: Error,
		Msg:  "失败",
	})
	c.Abort()
}

func FailMsg(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, Result{
		Code: Error,
		Msg:  msg,
	})
	c.Abort()
}

func Redirect(c *gin.Context, path string) {
	c.Redirect(http.StatusFound, path)
}
