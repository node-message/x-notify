package logic

import (
	"github.com/gin-gonic/gin"
	"nodemessage.com/x-notify/bootstrap/config"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/bootstrap/model"
	"nodemessage.com/x-notify/bootstrap/router"
	"nodemessage.com/x-notify/bootstrap/util"
)

func init() {
	router.Register(func(g *gin.Engine) {
		loginRouter := router.NewRouter("/system", g)
		loginRouter.Api(router.M_POST, "/login", login)
	})
}

func login(context *gin.Context) {
	token, err := util.TokenCreateJson([]byte(config.Properties().TokenKey), model.Context{
		Uuid: "111",
	})
	if err != nil {
		logger.Error("%v", err)
	}
	router.OkPayload(context, map[string]any{
		"token": token,
	})
}
