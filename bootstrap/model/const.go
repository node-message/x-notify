package model

const (
	TOKEN_NAME   = "token"
	AUTH_CONTEXT = "auth_context"
	HTML_LOGIN   = "/asset/pages/login.html"
	HTML_INDEX   = "/asset/index.html"
)
