package conf

import "nodemessage.com/x-notify/bootstrap/config"

type Properties struct {
	Mail   Mail
	Config config.Config
}

type Mail struct {
	UserEmail     string `mapstructure:"user_email"`
	PasswordEmail string `mapstructure:"password_email"`
	HostEmail     string `mapstructure:"host_email"`
	PortEmail     int    `mapstructure:"port_email"`
	AesKey        string `mapstructure:"aes_key"`
}
