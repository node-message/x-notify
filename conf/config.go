package conf

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/bootstrap/util"
	"os"
	"path/filepath"
	"text/template"
)

var (
	PROPERTIES = new(Properties)
)

var (
	configPath     = util.HomePath()
	configFile     = "xnotify"
	configFileName = configFile + ".ini"
	viperIns       *viper.Viper
)

func InitConfig() bool {
	confIniPath := filepath.Join(configPath, configFileName)
	if _, err := os.Stat(confIniPath); err != nil {
		logger.Error("请使用 init 初始化项目!")
		return false
	}
	viperIns = viper.New()
	viperIns.SetConfigName(configFile)
	viperIns.SetConfigType("ini")
	viperIns.AddConfigPath(configPath)
	if err := viperIns.ReadInConfig(); err != nil {
		logger.Error("%v", err)
		return false
	}
	if err := viperIns.Unmarshal(PROPERTIES); err != nil {
		logger.Error("%v", err)
		return false
	}
	viperIns.WatchConfig()
	viperIns.OnConfigChange(func(in fsnotify.Event) {
		logger.Info("配置文件发生变化: %v", in)
		if err := viperIns.Unmarshal(PROPERTIES); err != nil {
			logger.Error("%v", err)
			return
		}
		logger.Info("当前配置: %v", *PROPERTIES)
	})
	return true
}

// InitConkCli 初始化项目
func InitConkCli() error {
	confIniPath := filepath.Join(configPath, configFileName)
	if _, err := os.Stat(confIniPath); err != nil {
		os.MkdirAll(filepath.Dir(confIniPath), os.ModePerm)
		conkIniFile, err := os.Create(confIniPath)
		if err != nil {
			return fmt.Errorf("配置文件创建失败 : %v", err)
		}
		defer conkIniFile.Close()
		fs, err := template.ParseFS(IniConf, configFileName+".template")
		if err != nil {
			return fmt.Errorf("获取源配置文件失败 : %v", err)
		}
		err = fs.Execute(conkIniFile, map[string]any{
			"db_path": util.BinAbsPath(),
		})
		if err != nil {
			return fmt.Errorf("生成源配置文件失败 : %v", err)
		}
		return nil
	} else {
		logger.Info("配置文件已存在,若要重新生成,可将配置文件删除.")
		return nil
	}
}

func WriteConfig() error {
	return viperIns.WriteConfig()
}
