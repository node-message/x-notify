package main

import (
	"nodemessage.com/x-notify/bootstrap/config"
	"nodemessage.com/x-notify/conf"
	_ "nodemessage.com/x-notify/logic"
)
import "nodemessage.com/x-notify/bootstrap"

func main() {
	bootstrap.Run(func() config.Config {
		return conf.PROPERTIES.Config
	}, func() bool {
		return conf.InitConfig()
	})
}
