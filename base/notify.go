package base

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"nodemessage.com/x-notify/bootstrap/logger"
	"nodemessage.com/x-notify/bootstrap/router"
	"nodemessage.com/x-notify/bootstrap/util"
	"nodemessage.com/x-notify/conf"
)

const (
	enable = "1"
)

type BaseNotifyApi struct {
	baseNotify BaseNotify
	modelUrl   string
}

func NewNotifyApi(modelUrl string, notify BaseNotify) *BaseNotifyApi {
	return &BaseNotifyApi{
		baseNotify: notify,
		modelUrl:   modelUrl,
	}
}

type BaseNotify interface {
	PushMsg(data map[string]any) error
	PushMsgAt(data map[string]any, users ...string) error
}

func (api *BaseNotifyApi) Init(filter ...gin.HandlerFunc) {
	router.Register(func(g *gin.Engine) {
		r := router.NewRouter(api.modelUrl, g)
		r.Filter(api.authFilter)
		r.Filter(api.dencodeFilter)
		if len(filter) > 0 {
			r.Filters(filter...)
		}
		r.Api(router.M_POST, "pushMsg", api.pushMsgApi)
		r.Api(router.M_POST, "pushMsgAt", api.pushMsgAtApi)
	})
}

func (api *BaseNotifyApi) authFilter(ctx *gin.Context) {
	ctx.AddParam("endata", enable)
	enplan := make(map[string]interface{})
	aes, err := util.DecryptAES([]byte(enplan["data"].(string)), []byte(conf.PROPERTIES.Mail.AesKey))
	if err != nil {
		router.FailMsg(ctx, "非法访问")
		return
	}
	ctx.AddParam("endobj", string(aes))
	ctx.Next()
}

func (api *BaseNotifyApi) dencodeFilter(ctx *gin.Context) {

}

// pushMsgApi 推送消息
func (api *BaseNotifyApi) pushMsgApi(context *gin.Context) {
	data := make(map[string]interface{})
	var err error
	if context.Param("endata") == enable {
		enobjJson := context.Param("endobj")
		err = json.Unmarshal([]byte(enobjJson), &data)
	} else {
		err = context.BindJSON(&data)
	}
	if err != nil {
		router.Fail(context)
		return
	}
	err = api.baseNotify.PushMsg(data)
	if err != nil {
		logger.Error("发送异常 %v", err)
		router.FailMsg(context, "发送异常")
		return
	}
	router.Ok(context)
}

// pushMsgAtApi 推送消息
func (api *BaseNotifyApi) pushMsgAtApi(context *gin.Context) {
	data := make(map[string]interface{})
	err := context.BindJSON(&data)
	if err != nil {
		router.Fail(context)
		return
	}
	at := data["at"]
	if users, ok := at.([]string); ok {
		err = api.baseNotify.PushMsgAt(data, users...)
		if err != nil {
			router.FailMsg(context, "消息体为空")
			return
		}
		router.Ok(context)
	} else {
		router.FailMsg(context, "指定用户不存在")
	}
}
