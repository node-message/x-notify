BINARY_NAME=x-notify
BINARY_VERSION=1.0.1
RACE=-ldflags="-s -w -X "nodemessage.com/x-notify/bootstrap/build.Build=${BINARY_VERSION}""

.PHONY: all
all: win linux

.PHONY: linux
linux:
	set GOOS=linux&set GOARCH=amd64&set CGO_ENABLED=0&go build $(RACE) -o ./bin/linux/$(BINARY_NAME) ./main.go

.PHONY: win
win:
	set GOOS=windows&set GOARCH=amd64&go build $(RACE) -o ./bin/win/$(BINARY_NAME).exe ./main.go

install:
	go mod tidy

build: win linux