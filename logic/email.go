package logic

import (
	"fmt"
	"gopkg.in/gomail.v2"
	"nodemessage.com/x-notify/base"
	"nodemessage.com/x-notify/conf"
)

type ContentType string

const (
	TextHtml  ContentType = "text/html"
	TextPlain ContentType = "text/plain"
)

type EmailNotify struct {
}

func (e EmailNotify) PushMsg(data map[string]any) error {
	return e.sendMail("x-notify", data["to"].(string), data["subject"].(string), data["body"].(string), TextPlain)
}

func (e EmailNotify) PushMsgAt(data map[string]any, users ...string) error {
	return e.sendMail("x-notify", data["to"].(string), data["subject"].(string), data["body"].(string), TextPlain)
}

func (e EmailNotify) sendMail(name, to, subject, body string, mailType ContentType) error {
	m := gomail.NewMessage()
	m.SetHeader("From", fmt.Sprintf("%s<%s>", name, conf.PROPERTIES.Mail.UserEmail))
	m.SetHeader("Sender", conf.PROPERTIES.Mail.UserEmail)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody(string(mailType), body)
	d := gomail.NewDialer(conf.PROPERTIES.Mail.HostEmail, conf.PROPERTIES.Mail.PortEmail,
		conf.PROPERTIES.Mail.UserEmail, conf.PROPERTIES.Mail.PasswordEmail)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func init() {
	base.NewNotifyApi("email", EmailNotify{}).Init()
}
