package logic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"nodemessage.com/x-notify/base"
	"strings"
)

const (
	url_send = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s"
)

type QywxNotify struct {
}

func (q QywxNotify) PushMsg(data map[string]any) error {
	marshal, err := json.Marshal(data)
	if err != nil {

	}
	http.Post(fmt.Sprintf(url_send, ""), "application/json", strings.NewReader(string(marshal)))

	return nil
}

func (q QywxNotify) PushMsgAt(data map[string]any, users ...string) error {
	//TODO implement me
	panic("implement me")
}

func init() {
	base.NewNotifyApi("qywx", QywxNotify{}).Init()
}
